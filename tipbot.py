#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on 07.05.2019
@author: owebb
'''

from config import *
import logging
import time
from apscheduler.schedulers.background import BackgroundScheduler
from string import Formatter
import requests
import pyqrcode
from PIL import Image, ImageFont, ImageDraw
import subprocess
import json
from emoji import emojize
from telegram import ParseMode
from telegram.ext import (Updater, CommandHandler)
from builtins import str
from random import (random, randrange)

class AntiSpamFilter:

    def __init__(self, max_events, time_span):
        self.db = {}
        self.max_events = max_events
        self.time_span = time_span

    def verify(self, entity, add=True):
        if entity.lower() not in self.db:
            self.db[entity.lower()] = {
                "count": int(add),
                "start_time": time.time()
            }
            return True
        else:
            self.db[entity.lower()]["count"] += int(add)
            _count = self.db[entity.lower()]["count"]
            _start_time = self.db[entity.lower()]["start_time"]
            if _count > self.max_events:
                if (time.time() - _start_time) <= self.time_span:
                    return False
                else:
                    self.db[entity.lower()]["count"] = 0
                    self.db[entity.lower()]["start_time"] = time.time()
                    return True
            else:
                return True

_spam_filter = AntiSpamFilter(5, 60)

## Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

## Called by scheduler for checking incoming receive transactions from accounts wallet and stake transactions from stake wallet
def check_incoming_transactions():
    check_deposit_transactions()
    check_stake_transactions()

## Incoming transactions from users need to be tracked by moving transferred Pandacoins from users account to main account for staking support + adding to balance in json users file
def check_deposit_transactions():
    account_list = subprocess.run([accounts_wallet,"listaccounts","10"],stdout=subprocess.PIPE).stdout.strip().decode(encoding)
    accounts_json = json.loads(account_list)
    users_json = read_users_list()
    for user, user_balance in accounts_json.items():
        if len(user) > 0 and user_balance > 0.0:
            user_balance -= 5
            print("User: " + user)
            print("Balance: " + str(user_balance))
            if walletpassphrase == "":
                tx_id = subprocess.run([accounts_wallet,"sendfrom",user,staking_wallet_address,str(user_balance)],stdout=subprocess.PIPE).stdout.strip().decode(encoding)
            else:
                subprocess.run([accounts_wallet,"walletpassphrase", walletpassphrase,"1","false"],stdout=subprocess.PIPE)
                tx_id = subprocess.run([accounts_wallet,"sendfrom",user,staking_wallet_address,str(user_balance),"1"],stdout=subprocess.PIPE).stdout.strip().decode(encoding)
            print("tx_id: " + tx_id)
            if len(tx_id) == 64:
                if user in users_json:
                    users_json[user.lower()] += user_balance
                else:
                    users_json[user.lower()] = user_balance
                msg = "User id: {0} - User balance: {1} - Tx msg: {2}".format(user, user_balance, tx_id)
                print(msg)
                user_account_balance = subprocess.run([accounts_wallet,"getbalance",user,"10"],stdout=subprocess.PIPE).stdout.strip().decode(encoding)
                if float(user_account_balance) > 0.0:
                    subprocess.run([accounts_wallet,"move",user,"",user_account_balance],stdout=subprocess.PIPE)
            else:
                msg = "Something went wrong! User id: {0} - User balance: {1} - Tx msg: {2}".format(user, user_balance, tx_id)
                print(msg)
    write_users_list(users_json)

## For a successful stake from main account Pandacoins are distributed to all other accounts according to their balance
def check_stake_transactions():
    tx_ids_from_staking_wallet = rpc_connect("listtransactions", ["*", 99999])
    staking_tx_json = read_staking_tx_list()
    users_json = read_users_list()
    for tx in tx_ids_from_staking_wallet['result']:
        if tx['category'] == 'stake' and tx['txid'] not in staking_tx_json:
            staking_tx_json[tx['txid']] = tx['amount']
            users_balance = 0.0
            for user, user_balance in users_json.items():
                users_balance += user_balance
            print("users total balance: " + str(users_balance))
            for user, user_balance in users_json.items():
                if user_balance > 0.0:
                    print("current balance user: " + str(user_balance))
                    user_balance += (user_balance / users_balance) * tx['amount']
                    users_json[user.lower()] = user_balance
                    print("new balance user: " + str(user_balance))
    write_staking_tx_list(staking_tx_json)
    write_users_list(users_json)

def commands(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    user = update.message.from_user.username
    if user is None:
        user = "!"
    else:
        user = " @" + user
    wave_emoji = get_emoji(":wave:")
    commands_msg = "Hello{0} {1} Initiating commands /tip & /withdraw have a specific format. Use them like so: \n \n Parameters: \n <code>username</code> = target user to tip (starting with @) \n <code>amount</code> = amount of Pandacoin to utilize \n <code>address</code> = Pandacoin address to withdraw to \n \n Tipping format: \n <code>/tip @username amount</code> \n \n Withdrawing format: \n <code>/withdraw address amount</code> \n \n Need more help? -> /helptip".format(user, wave_emoji)
    send_text_msg(update, context, commands_msg)

def help(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    help_msg = "The following commands are at your disposal: /commands /deposit /tip /donate /withdraw /balance /exchanges /contrat /price /marketcap /statistics /leaderboard /hi /feedbamboo /joke /bonk \n \nExamples: \n<code>/tip @amDOGE 100</code>\n-> send a tip of 100 Pandacoins to our project lead amDOGE\n-> support Pandacoin team by donating them 100 Pandacoins\n<code>/withdraw {0} 100</code>\n-> send 100 Pandacoins to a specific address (in this example: dev fund raising address which is also used for /donate)".format(dev_fund_address)
    send_text_msg(update, context, help_msg)

def deposit(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    user_username = update.message.from_user.username
    user_first_name = update.message.from_user.first_name
    users_json = read_users_list()
    if user_username is None:
        no_user_msg = "Hey {0}, please set a Telegram username in your profile settings first.\nWith your unique username you can access your <b>Telegram Pandacoin wallet</b>. If you change your username you might lose access to your Pandacoins! This wallet is separated from any other wallets and cannot be connected to other ones!".format(user_first_name)
        send_text_msg(update, context, no_user_msg)
    else:
        if user_username.lower() not in users_json:
            create_new_wallet(update, context, user_first_name, user_username, users_json)
            fetch_deposit_address(update, context, user_username)
        else:
            fetch_deposit_address(update, context, user_username)

def tip(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    user = update.message.from_user.username
    user_first_name = update.message.from_user.first_name
    user_input = update.message.text[5:].strip()
    users_json = read_users_list()
    if user_input == "":
        no_parameters = "There is something missing! See /helptip for an example."
        send_text_msg(update, context, no_parameters)
    elif user is None:
        no_user_msg = "Hey {0}, please set a Telegram username in your profile settings first.\nWith your unique username you can access your <b>Telegram Pandacoin wallet</b>. If you change your username you might lose access to your Pandacoins! This wallet is separated from any other wallets and cannot be connected to other ones!".format(user_first_name)
        send_text_msg(update, context, no_user_msg)
    else:
        target = user_input.split(" ")[0]
        amount = float(user_input.split(" ")[1])
        if target == bot_name or target == "@" + bot_name:
            hodl_msg = "HODL."
            send_text_msg(update, context, hodl_msg)
        elif "@" in target and user.lower() in users_json:
            target = target[1:]
            balance = users_json[user.lower()]
            if balance < amount:
                insufficient_funds_msg = "@{0} you have insufficient funds.".format(user)
                send_text_msg(update, context, insufficient_funds_msg)
            elif target == user:
                self_tip_msg = "You can't tip yourself silly."
                send_text_msg(update, context, self_tip_msg)
            elif amount > 0.0:
                users_json[user.lower()] -= amount
                if target.lower() not in users_json:
                    create_new_wallet(update, context, target, target, users_json)
                users_json[target.lower()] += amount
                tip_msg = "@{0} tipped @{1} {2} PND".format(user, target, amount)
                if "69" in str(amount):
                    tip_msg += ". Noice!"
                send_text_msg(update, context, tip_msg)
            else:
                tip_msg = "I see what you did there!"
                send_text_msg(update, context, tip_msg)
        else:
            wrong_format_msg = "Error that user is not applicable. Need help? -> /helptip"
            send_text_msg(update, context, wrong_format_msg)
    write_users_list(users_json)

def balance(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    if update.effective_chat is None:
        _chat_type = "private"
    elif update.effective_chat.type == "private":
        _chat_type = "private"
    else:
        _chat_type = "group"
    if _chat_type == "group":
        return send_text_msg(update, context, "You can only ask me this in private.")
    user_username = update.message.from_user.username
    user_first_name = update.message.from_user.first_name
    # Get JSON data
    json_obj = read_users_list()
    if user_username is None:
        no_user_msg = "Hey {0}, please set a Telegram username in your profile settings first.\nWith your unique username you can access your <b>Telegram Pandacoin wallet</b>. If you change your username you might lose access to your Pandacoins! This wallet is separated from any other wallets and cannot be connected to other ones!".format(user_first_name)
        send_text_msg(update, context, no_user_msg)
    elif user_username.lower() in json_obj:
        user_balance = decimal_round_down(json_obj[user_username.lower()])
        user_id = str(update.effective_user.id)
        if user_id in json_obj:
            user_balance += decimal_round_down(json_obj[user_id])
            json_obj[user_username.lower()] = user_balance
            del json_obj[user_id]
            write_users_list(json_obj)
        price_info = get_market_data_info_from_coingecko()
        price_usd = float(price_info[1])
        fiat_balance = user_balance * price_usd
        fiat_balance = "{0:,.3f}".format(fiat_balance)
        balance_msg = "@{0} your current balance is: Ᵽ<code>{1}</code> ≈ $<code>{2}</code>".format(user_username, user_balance, fiat_balance)
        send_text_msg(update, context, balance_msg)
    else:
        create_new_wallet(update, context, user_first_name, user_username, json_obj)

def create_new_wallet(update, context, user_first_name, user_username, json_obj):
    json_obj[user_username.lower()] = 0.0
    write_users_list(json_obj)
    balance_msg = "Hi {0}! I have just created a wallet which is associated with your unique Telegram username @{1}\n-> If you need help just hit /helptip or /commands to get started!".format(user_first_name, user_username)
    send_text_msg(update, context, balance_msg)

def fetch_deposit_address(update, context, user_username):
    user_accountaddress = subprocess.run([accounts_wallet,"getaccountaddress",user_username.lower()],stdout=subprocess.PIPE).stdout.strip().decode(encoding)
    qrcode_png = create_qr_code(user_accountaddress, user_username)
    deposit_msq = "@{0} your depositing address is: `{1}`\n-> Get the [Android Wallet here]({2})!".format(user_username, user_accountaddress, android_wallet_url)
    send_photo_msg(update, context, qrcode_png, deposit_msq)

def price(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    price_info = get_market_data_info_from_coingecko()
    price_btc = price_info[0]
    price_usd = price_info[1]
    price_change_percentage_24h = price_info[2]
    if price_change_percentage_24h < 0:
        change_symbol = "-"
        price_change_percentage_24h = -price_change_percentage_24h
    elif price_change_percentage_24h > 0:
        change_symbol = "+"
    else:
        change_symbol = ""
    price_change_percentage_24h = "{:.2f}".format(price_change_percentage_24h) + "%"
    if change_symbol != "":
        price_msg = "1 Pandacoin is valued at $<code>{0}</code> Δ {1}<code>{2}</code> ≈ ₿<code>{3}</code>".format(price_usd,change_symbol,price_change_percentage_24h,price_btc)
    else:
        price_msg = "1 Pandacoin is valued at $<code>{0}</code> ≈ ₿<code>{1}</code>".format(price_usd,price_btc)
    send_text_msg(update, context, price_msg)

def newDonation(update, context, allowed=False):
    user_id = update.message.from_user.username
    if allowed:
        user_input_user_id = "@" + user_id
        user_input_user_first_name = update.message.from_user.first_name
        user_input_user_last_name = update.message.from_user.last_name
        if user_input_user_last_name == None:
            user_input_user_display_name = user_input_user_first_name
        else:
            user_input_user_display_name = user_input_user_first_name + " " + user_input_user_last_name
        user_input_user_amount = update.message.text[8:].strip().split(" ")[0]
        addDonation(update, context, user_input_user_id, user_input_user_display_name, user_input_user_amount)
    elif user_id in admin_list:
        user_input = update.message.text.partition(' ')[2]
        user_input_user_id = ''.join(user_input.partition(" ")).split(" ")[0]
        user_input_user_display_name_and_amount = ''.join(user_input.partition(" ")).split(user_input_user_id)[1]
        user_input_user_display_name = user_input_user_display_name_and_amount[:user_input_user_display_name_and_amount.rfind(' ')].strip()
        user_input_user_amount = user_input_user_display_name_and_amount[user_input_user_display_name_and_amount.rfind(' '):].strip()
        addDonation(update, context, user_input_user_id, user_input_user_display_name, user_input_user_amount)
    else:
        send_user_not_allowed_text_msg(update, context)

def addDonation(update, context, user_input_user_id, user_input_user_display_name, user_input_user_amount):
    # Get JSON data to store donation
    json_obj = read_donors_list()
    user_key = user_input_user_id + " " + user_input_user_display_name
    tada_emoji = get_emoji(":tada:")
    if user_key in json_obj:
        donation_amount = json_obj[user_key]
        new_donation = float(user_input_user_amount)
        new_balance = donation_amount + new_donation
        json_obj[user_key] = new_balance
        donation_msg = "Donation from user {0} increased from {1} to {2} PND {3}".format(user_input_user_id, donation_amount, new_balance, tada_emoji)
    else:
        json_obj[user_key] = float(user_input_user_amount)
        donation_msg = "Added new donation of {0} PND from user {1} to leaderboard {2}".format(user_input_user_amount, user_input_user_id, tada_emoji)
    write_donors_list(json_obj)
    send_text_msg(update, context, donation_msg)

def removeDonor(update, context):
    # Remove donor from leaderboard
    user_id = update.message.from_user.username
    if user_id in admin_list:
        json_obj = read_donors_list()
        user_input = update.message.text.partition(' ')[2]
        user_input_user_id = ''.join(user_input.partition(" ")).split(" ")[0]
        user_input_user_display_name = ''.join(user_input.partition(" ")).split(user_input_user_id)[1]
        user_key = user_input_user_id + " " + user_input_user_display_name.strip()
        if user_key in json_obj:
            del json_obj[user_key]
            remove_msg = "Donor '{0}' was removed from leaderboard.".format(user_key)
        else:
            neutral_face_emoji = get_emoji(":neutral_face:")
            remove_msg = "Sorry but donor '{0}' was not found on leaderboard {1}".format(user_key, neutral_face_emoji)
        write_donors_list(json_obj)
        send_text_msg(update, context, remove_msg)
    else:
        send_user_not_allowed_text_msg(update, context)

def leaderboard(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    user_id = update.message.from_user.username
    user_input = update.message.text[12:].strip().lower()
    json_obj = read_donors_list()
    counter = 0
    json_obj_size = str(len(json_obj))
    if user_id is not None and user_input == "position":
        for key, value in sorted(json_obj.items(), key=lambda item: item[1], reverse=True):
            counter += 1
            if user_id in key:
                leaderboard_msg = "Hey @{0}, your current position in leaderboard donation list is: {1} out of {2}".format(user_id, counter, json_obj_size)
                break;
    else:
        rank = {1 : "🥇", 2 : "🥈", 3 : "🥉", 4 : "🎖", 5 : "🎖", 6 : "🎖", 7 : "🎖", 8 : "🎖", 9 : "🎖", 10 : "🎖"}
        tada_emoji = get_emoji(":tada:")
        leaderboard_msg = "<b>Top {0} Pandacoin donors</b> {1}\n".format(leaderboard_max_entries, tada_emoji)
        for key, value in sorted(json_obj.items(), key=lambda item: item[1], reverse=True):
            counter += 1
            if counter > leaderboard_max_entries:
                break
            else:
                username = key.split(" ")[0]
                display_name = key.replace(username, "")
                value = "{0:,.6f}".format(value).rstrip("0").rstrip(".")
                leaderboard_msg += rank[counter] + " " + username[1:] + " |" + display_name + "\n-> Ᵽ<code>" + value + "</code>\n"
        leaderboard_msg += "_____________________________\n"
        #leaderboard_msg += "<b>We also want to thank the following anonymous donors:</b>\n"
        #leaderboard_msg += "🥇 Anonymous 1 -> Ᵽ<code>1,500,000</code>\n"
        #leaderboard_msg += "🥈 Anonymous 2 -> Ᵽ<code>1,000,000</code>\n"
        #leaderboard_msg += "🥉 Anonymous 3 -> Ᵽ<code>500,000</code>\n"
        #leaderboard_msg += "_____________________________\n"
        leaderboard_msg += "‼ Use /donate 'amount of PND' to support Pandacoin development team and you might be on this list!"
    send_text_msg(update, context, leaderboard_msg)

def donate(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    user_input = update.message.text.replace(bot_name,"")
    user_input = user_input[8:].strip()
    withdraw_successful = False
    if user_input == "":
        qrcode_png = create_qr_code(dev_fund_address, "PND Devs")
        donate_qr_msg = "`{0}`".format(dev_fund_address)
        donate_text_msg = "Any donations are highly appreciated 👍\n-> Hit /leaderboard to get a list of top 10 contributors.".format(dev_fund_address)
        send_photo_msg(update, context, qrcode_png, donate_qr_msg)
        send_text_msg(update, context, donate_text_msg)
    else:
        withdraw_successful = withdraw(update, context)
        if withdraw_successful:
            newDonation(update, context, True)

def withdraw(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    user = update.message.from_user.username
    user_first_name = update.message.from_user.first_name
    user_input = update.message.text.replace(bot_name, "")
    withdraw_successful = False
    users_json = read_users_list()
    if user_input.startswith("/donate"):
        user_input = user_input[8:].strip()
    else:
        user_input = user_input[10:].strip()
    if user_input == "":
        no_parameters_msg = "There is something missing! See /helptip for an example."
        send_text_msg(update, context, no_parameters_msg)
    elif user is None:
        no_user_msg = "Hey {0}, please set a Telegram username in your profile settings first.\nWith your unique username you can access your <b>Telegram Pandacoin wallet</b>. If you change your username you might lose access to your Pandacoins! This wallet is separated from any other wallets and cannot be connected to other ones!".format(user_first_name)
        send_text_msg(update, context, no_user_msg)
    else:
        if update.message.text.startswith("/donate"):
            address = dev_fund_address
            amount = float(user_input)
        else:
            address = user_input.split(" ")[0]
            amount = float(user_input.split(" ")[1])
        balance = users_json[user.lower()]
        if amount < 0.0:
            negative_amount_msg = "I see what you did there!"
            send_text_msg(update, context, negative_amount_msg)
        elif balance < amount:
            neutral_face_emoji = get_emoji(":neutral_face:")
            empty_balance_msg = "Sorry @{0}, but you have insufficient funds {1}".format(user, neutral_face_emoji)
            send_text_msg(update, context, empty_balance_msg)
        elif amount > 1:
            tx_id = rpc_connect("sendtoaddress", [address, amount])['result']
            if len(tx_id) == 64:
                withdraw_successful = True
                withdraw_msg = "@{0} has successfully withdrawn Ᵽ<code>{1}</code> to address <code>{2}</code> (transaction: {3}/tx.dws?{4})".format(user, amount, address, os.environ['EXPLORER_BASE_URL'], tx_id)
                users_json[user.lower()] -= amount
                write_users_list(users_json)
                send_text_msg(update, context, withdraw_msg)
        else:
            withdraw_msg = "Sorry @{0} but the amount for withdrawals has to be more than Ᵽ<code>1</code>".format(user)
            send_text_msg(update, context, withdraw_msg)
    return withdraw_successful

def hi(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    user = update.message.from_user.username
    hi_msg = "Hello @{0}, how are you doing today?".format(user)
    send_text_msg(update, context, hi_msg)

def feedbamboo(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    user = update.message.from_user.username
    if user:
        user = "@{0}".format(user)
    else:
        user = update.message.from_user.first_name
    panda_emoji = get_emoji(":panda:")
    bamboo_emoji = get_emoji(":bamboo:")
    bamboo_msg = "Thank you, {0}! MONCH! {1}{2}".format(user, panda_emoji, bamboo_emoji)
    send_text_msg(update, context, bamboo_msg)

def bonk(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    user = update.message.from_user.username
    user_input = update.message.text[6:].strip()
    if user:
        user = "@{0}".format(user)
    else:
        user = update.message.from_user.first_name
    target = user_input.split(" ")[0]
    if target == "douglas" or target == "Douglas":
        msg = "{0}, for your own safety, you cannot bonk Douglas. He is too powerful.".format(user)
    elif target == user:
        msg = "{0} just bonked themself.".format(user)
    elif target == bot_name or target == "@" + bot_name:
        msg = "OUCH, {0}! That hurt :(".format(user)
    else:
        msg = "{0} just bonked {1}!".format(user, target)
    send_text_msg(update, context, msg)

def joke(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    jokes = [
        "What's invisible and smells like bamboo?\n\nPanda farts.",
        "What goes black, white, black, white, black, white?\n\nA panda rolling down a hill.",
        "What's black and white and goes round and round?\n\nA panda stuck in a revolving door.",
        "Did you hear about the pandas that were in a food fight?\n\nThey all got Bambooboos.",
        "What do you call a male panda?\n\nAmanda.",
        "How do pandas get to the hospital?\n\nThe bamboolance.",
        "Why should you never let a panda into a chemistry lab?\n\nIt will create pandamonium.",
        "How does a panda make his pancakes in the morning?\n\nWith a pan...duh.",
        "Why did the panda travel to Wall Street?\n\nTo buy bamboo stalks.",
        "What do pandas say to scare each other on Halloween?\n\nBamBOO!",
        "How do pandas manage their back pain?\n\nPanda-dol tablets.",
        "Why are pandas so lazy?\n\nThe only do the BEAR minimum.",
    ]
    joke_msg = jokes[randrange(len(jokes)-1)]
    send_text_msg(update, context, joke_msg)

def contrat(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    user = update.message.from_user.username
    if user:
        user = "@{0}".format(user)
    else:
        user = update.message.from_user.first_name
    panda_emoji = get_emoji(":panda:")
    contrat_msg = "Sorry {0}, there is no contrat. Try /exchanges to see how you can buy Pandacoin (PND) {1}".format(user, panda_emoji)
    send_text_msg(update, context, contrat_msg)

def marketcap(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    price_info = get_market_data_info_from_coingecko()
    marketcap_btc = price_info[3]
    marketcap_usd = "{0:,.0f}".format(price_info[4])
    marketcap_msg = "The current market cap of Pandacoin is valued at $<code>{0}</code> ≈ ₿<code>{1}</code>".format(marketcap_usd, marketcap_btc)
    send_text_msg(update, context, marketcap_msg)

def statistics(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    # sum of staking from staking tx json file
    stakings_json = read_staking_tx_list()
    number_of_stakes = 0
    total_stake_amount = 0.0
    for tx, stake_amount in stakings_json.items():
        number_of_stakes += 1
        total_stake_amount += stake_amount
    dev_fund_balance = str(requests.get(dev_fund_balance_api_url).content)
    dev_fund_balance = dev_fund_balance.replace("'","").replace("b","")
    dev_fund_balance = "{0:,.6f}".format(float(dev_fund_balance)).rstrip("0").rstrip(".")
    getinfo = subprocess.run([accounts_wallet,"gettxoutsetinfo"],stdout=subprocess.PIPE).stdout.strip().decode(encoding)
    getbalance = rpc_connect("getbalance", [])['result']
    #listaccounts = subprocess.run([accounts_wallet,"listaccounts","10"],stdout=subprocess.PIPE).stdout.strip().decode(encoding)
    getinfo_json = json.loads(getinfo)
    #listaccounts_json = json.loads(listaccounts)
    block_height = getinfo_json["height"]
    money_supply = getinfo_json["total_amount"]
    block_height = "{0:,.0f}".format(block_height)
    money_supply = "{0:,.0f}".format(money_supply)
    total_balance = "{0:,.6f}".format(getbalance).rstrip("0").rstrip(".")
    #total_users = len(listaccounts_json) - 1
    next_party_block = str(int(block_height[:block_height.find(",")]) + 1)
    next_party_block = next_party_block.ljust(len(str(getinfo_json["height"])), "0")
    diff = int(next_party_block) - getinfo_json["height"]
    time_to_party = strfdelta(diff * 10, "{D:02}d {H:02}h {M:02}m", inputtype="m")
    diff = "{0:,.0f}".format(diff)
    next_party_block = "{0:,.0f}".format(int(next_party_block))
    tada_emoji = get_emoji(":tada:")
    check_mark_emoji = emojize(":white_check_mark:", use_aliases=True)
    block_height_msg = "{0} With current block height of <code>{1}</code> there are <code>{2}</code> left to block <code>{3}</code> {4} -> Countdown: <code>{5}</code>".format(check_mark_emoji, block_height, diff, next_party_block, tada_emoji, time_to_party)
    netstake_weight_msg = "{0} There are currently a total of <code>{1}</code> Pandacoins in existence".format(check_mark_emoji, money_supply)
    accounts_msg = "{0} Our famous Telegram tipping bot {1} is currently holding <code>{2}</code> Pandacoins".format(check_mark_emoji, bot_name, total_balance)
    stake_msg = "{0} Wallet of Telegram tipping bot has staked {1} time(s) with a total of <code>{2}</code> Pandacoins".format(check_mark_emoji, number_of_stakes, total_stake_amount)
    dev_fund_balance_msg = "{0} Balance of development funding address: Ᵽ<code>{1}</code>".format(check_mark_emoji, dev_fund_balance)
    send_text_msg(update, context, block_height_msg + "\n" + netstake_weight_msg + "\n" + accounts_msg + "\n" + stake_msg + "\n" + dev_fund_balance_msg)

def tw(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    user_input = update.message.text[3:].strip()
    target = user_input.split(" ")[0]
    msg = "Pandacoin is <b>not</b> supported via Trust Wallet.\nFurther, there are <b>no contract addresse(s)</b> to be found on ETH, BSC or similiar chains.\nIs that even possible you ask? With Pandacoin it is!\nWe do however offer a selection of custom wallets over at https://pandacoin.tech.\nPlease also check the pinned message(s) for more information."
    if user_input and "@" in target:
        msg = "Welcome, {0}!\n{1}".format(target, msg)
    send_text_msg(update, context, msg)

def exchanges(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    chart_emoji = get_emoji(":chart_with_upwards_trend:")
    altmarkets_base = "https://v2.altmarkets.io/trading/pnd"
    bitubu_base = "https://bitubu.com/en/trading/pnd"
    dextrade_base = "https://dex-trade.com/spot/trading/PND"
    msg = "Pandacoin can be traded on these exchanges:"
    msg += "\n{0}<b>Dex-Trade</b>".format(chart_emoji)
    msg += "\n<a href='{0}USDT'>PND/USDT</a>".format(dextrade_base)
    #msg += "\n<a href='{0}BTC'>PND/BTC</a>".format(dextrade_base)
    #msg += "\n<a href='{0}ETH'>PND/ETH</a>".format(dextrade_base)
    msg += "\n\n{0}<b>Altmarkets</b>".format(chart_emoji)
    msg += "\n<a href='{0}doge'>PND/DOGE</a>".format(altmarkets_base)
    msg += "\n<a href='{0}usdt'>PND/USDT</a>".format(altmarkets_base)
    msg += "\n<a href='{0}ltc'>PND/LTC</a>".format(altmarkets_base)
    msg += "\n<a href='{0}btc'>PND/BTC</a>".format(altmarkets_base)
    msg += "\n\n{0}<b>Bitubu</b>".format(chart_emoji)
    msg += "\n<a href='{0}usdt'>PND/USDT</a>".format(bitubu_base)
    msg += "\n<a href='{0}btc'>PND/BTC</a>".format(bitubu_base)
    send_text_msg(update, context, msg)

def gamble(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    user_input = update.message.text[7:].strip()
    user = update.message.from_user.username
    if user_input == "":
        no_parameters_msg = "You cannot gamble with no input!"
        send_text_msg(update, context, no_parameters_msg)
        return
    if user is None:
        no_user_msg = "Hey {0}, please set a Telegram username in your profile settings first.\nWith your unique username you can access your <b>Telegram Pandacoin wallet</b>. If you change your username you might lose access to your Pandacoins! This wallet is separated from any other wallets and cannot be connected to other ones!".format(user_first_name)
        send_text_msg(update, context, no_user_msg)
        return
    users_json = read_users_list()
    input_amount = float(user_input.split()[0])
    input_amount = round(input_amount, 6)
    if input_amount <= 0.0:
        return
    if users_json[user.lower()] < input_amount:
        insufficient_funds_msg = "@{0} you have insufficient funds.".format(user)
        send_text_msg(update, context, insufficient_funds_msg)
        return
    msg = "@{0} just".format(user)
    if random() > 0.55:
        output_amount = input_amount * 2
        output_amount = round(output_amount, 6)
        if output_amount <= 0.0:
            return
        if output_amount > (users_json[bot_name.lower()] + input_amount):
            insufficient_funds_msg = "Sorry @{0}, you cannot wager that much at the moment.".format(user)
            send_text_msg(update, context, insufficient_funds_msg)
            return
        users_json[user.lower()] += input_amount
        users_json[bot_name.lower()] -= input_amount
        tada_emoji = get_emoji(":tada:")
        msg += " won {0} PND {1}".format(input_amount, tada_emoji)
    else:
        users_json[user.lower()] -= input_amount
        if users_json[user.lower()] <= 0.0:
            users_json[user.lower()] = 0.0
        users_json[bot_name.lower()] += input_amount
        msg += " lost {0} PND".format(input_amount)
    msg += "\nThey now have {0} PND".format(decimal_round_down(users_json[user.lower()]))
    write_users_list(users_json)
    send_text_msg(update, context, msg)

def rain(update, context):
    if not _spam_filter.verify(str(update.effective_user.id)):
        return
    send_text_msg(update, context, "The /rain command is currently not implemented, sorry.")

def get_market_data_info_from_coingecko():
    response = requests.get(market_data_origin)
    raw_data = response.json()
    price_btc = '{:.8f}'.format(raw_data['market_data']['current_price']['btc'])
    price_usd = '{:.6f}'.format(raw_data['market_data']['current_price']['usd'])
    price_change_percentage_24h = raw_data['market_data']['price_change_percentage_24h']
    market_cap_btc = raw_data['market_data']['market_cap']['btc']
    market_cap_usd = raw_data['market_data']['market_cap']['usd']
    return[price_btc, price_usd, price_change_percentage_24h, market_cap_btc, market_cap_usd]

def rpc_connect(method, params):
    url = "http://" + staking_wallet_rpc_ip +":" + staking_wallet_rpc_port + "/"
    payload = json.dumps({"method": method, "params": params})
    headers = {'content-type': "application/json", 'cache-control': "no-cache"}
    try:
        response = requests.request("POST", url, data=payload, headers=headers, auth=(staking_wallet_rpc_user, staking_wallet_rpc_pw))
        if response.text.startswith("{"):
            return json.loads(response.text)
        else:
            return response.text
    except:
        print("No response from server. Check if Pandacoin core wallet is running on " + staking_wallet_rpc_ip)

def send_user_not_allowed_text_msg(update, context):
    telegram_admin_user_list = ""
    for admin_user in admin_list:
        telegram_admin_user_list += " @" + admin_user
    admin_msg = "This function is restricted to following admins:{0}".format(telegram_admin_user_list)
    send_text_msg(update, context, admin_msg)

def send_text_msg(update, context, msg):
    context.bot.send_message(chat_id=update.message.chat_id, text=msg, parse_mode=ParseMode.HTML, disable_web_page_preview=True)
    print("chat-id: " + str(update.message.chat_id))

def send_photo_msg(update, context, photo, caption):
    context.bot.send_photo(chat_id=update.message.chat_id, photo=open(photo, "rb"), caption=caption, parse_mode=ParseMode.MARKDOWN)

def get_emoji(emoji_shortcode):
    emoji = emojize(emoji_shortcode, use_aliases=True)
    return emoji

def create_qr_code(value, user_username):
    # Generate the qr code and save as png
    qrcode_png = value + ".png"
    qrcode_prefix = "pandacoin:"
    qrobj = pyqrcode.QRCode(qrcode_prefix + value, error = "H")
    qrobj.png(qrcode_png, scale=10, quiet_zone=1)
    # Now open that png image to put the logo
    img = Image.open(qrcode_png)
    img = img.convert("RGBA")
    width, height = img.size
    # Open the logo image and  define how big the logo we want to put in the qr code png
    logo = Image.open(qrcode_logo_img)
    logo_size = 80
    title_font = ImageFont.truetype('/usr/share/fonts/truetype/noto/NotoSansMono-Bold.ttf', 10)
    title_text = user_username
    image_editable = ImageDraw.Draw(logo)
    image_editable.text((0, 67), title_text, (0, 136, 204), font=title_font)
    # Calculate logo size and position
    xmin = ymin = int((width / 2) - (logo_size / 2))
    xmax = ymax = int((width / 2) + (logo_size / 2))
    logo = logo.resize((xmax - xmin, ymax - ymin))
    # put the logo in the qr code and save image
    img.paste(logo, (xmin, ymin, xmax, ymax))
    img.save(image_home + qrcode_png)
    return image_home + qrcode_png

def strfdelta(tdelta, fmt="{D:02}d {H:02}h {M:02}m {S:02}s", inputtype="timedelta"):
    """Convert a datetime.timedelta object or a regular number to a custom-
    formatted string, just like the stftime() method does for datetime.datetime
    objects.

    The fmt argument allows custom formatting to be specified.  Fields can 
    include seconds, minutes, hours, days, and weeks.  Each field is optional.

    Some examples:
        '{D:02}d {H:02}h {M:02}m {S:02}s' --> '05d 08h 04m 02s' (default)
        '{W}w {D}d {H}:{M:02}:{S:02}'     --> '4w 5d 8:04:02'
        '{D:2}d {H:2}:{M:02}:{S:02}'      --> ' 5d  8:04:02'
        '{H}h {S}s'                       --> '72h 800s'

    The inputtype argument allows tdelta to be a regular number instead of the  
    default, which is a datetime.timedelta object.  Valid inputtype strings: 
        's', 'seconds', 
        'm', 'minutes', 
        'h', 'hours', 
        'd', 'days', 
        'w', 'weeks'
    """

    # Convert tdelta to integer seconds.
    if inputtype == "timedelta":
        remainder = int(tdelta.total_seconds())
    elif inputtype in ["s", "seconds"]:
        remainder = int(tdelta)
    elif inputtype in ["m", "minutes"]:
        remainder = int(tdelta)*60
    elif inputtype in ["h", "hours"]:
        remainder = int(tdelta)*3600
    elif inputtype in ["d", "days"]:
        remainder = int(tdelta)*86400
    elif inputtype in ["w", "weeks"]:
        remainder = int(tdelta)*604800

    f = Formatter()
    desired_fields = [field_tuple[1] for field_tuple in f.parse(fmt)]
    possible_fields = ("W", "D", "H", "M", "S")
    constants = {"W": 604800, "D": 86400, "H": 3600, "M": 60, "S": 1}
    values = {}
    for field in possible_fields:
        if field in desired_fields and field in constants:
            values[field], remainder = divmod(remainder, constants[field])
    return f.format(fmt, **values)

def read_staking_tx_list():
    return read_json_file(staking_json_file)

def write_staking_tx_list(json_obj):
    return write_json_file(staking_json_file, json_obj)

def read_tippings_list():
    return read_json_file(tippings_json_file)

def write_tippings_list(json_obj):
    return write_json_file(tippings_json_file, json_obj)

def read_users_list():
    return read_json_file(users_json_file)

def write_users_list(json_obj):
    return write_json_file(users_json_file, json_obj)

def read_donors_list():
    return read_json_file(donors_json_file)

def write_donors_list(json_obj):
    return write_json_file(donors_json_file, json_obj)

def read_json_file(filename):
    json_file = open(filename, 'r')
    try:
        json_obj = json.load(json_file)
    finally:
        json_file.close()
    return json_obj

def write_json_file(filename, json_obj):
    json_file = open(filename, 'w')
    try:
        json.dump(json_obj, json_file, indent=4)
    finally:
        json_file.close()

def decimal_round_down(input):
    if type(input) == float:
        str_input = str(input)
    decimal_places = str_input[::-1].find('.')
    if int(decimal_places) > 6:
        result = round(float(str_input) - 0.000001, 6)
    else:
        result = float(str_input)
    return result

def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)

def main():
    ## Main Scheduler setup for running tasks
    scheduler = BackgroundScheduler()
    scheduler.add_job(check_incoming_transactions, 'interval', seconds=30)
    scheduler.start()

    ## Telegram bot dispatcher and handlers
    # Create the Updater and pass it your bot's token.
    updater = Updater(token=bot_token, use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add command handlers
    start_handler = CommandHandler("start", commands)
    dispatcher.add_handler(start_handler)

    commands_handler = CommandHandler("commands", commands)
    dispatcher.add_handler(commands_handler)

    statistics_handler = CommandHandler("statistics", statistics)
    dispatcher.add_handler(statistics_handler)

    hi_handler = CommandHandler("hi", hi)
    dispatcher.add_handler(hi_handler)

    feedbamboo_handler = CommandHandler("feedbamboo", feedbamboo)
    dispatcher.add_handler(feedbamboo_handler)

    contrat_handler = CommandHandler("contrat", contrat)
    dispatcher.add_handler(contrat_handler)

    bonk_handler = CommandHandler("bonk", bonk)
    dispatcher.add_handler(bonk_handler)

    joke_handler = CommandHandler("joke", joke)
    dispatcher.add_handler(joke_handler)

    donate_handler = CommandHandler("donate", donate)
    dispatcher.add_handler(donate_handler)

    withdraw_handler = CommandHandler("withdraw", withdraw)
    dispatcher.add_handler(withdraw_handler)

    marketcap_handler = CommandHandler("marketcap", marketcap)
    dispatcher.add_handler(marketcap_handler)

    deposit_handler = CommandHandler("deposit", deposit)
    dispatcher.add_handler(deposit_handler)

    price_handler = CommandHandler("price", price)
    dispatcher.add_handler(price_handler)

    tip_handler = CommandHandler("tip", tip)
    dispatcher.add_handler(tip_handler)

    balance_handler = CommandHandler("balance", balance)
    dispatcher.add_handler(balance_handler)

    help_handler = CommandHandler("helptip", help)
    dispatcher.add_handler(help_handler)

    newDonation_handler = CommandHandler("newDonation", newDonation)
    dispatcher.add_handler(newDonation_handler)

    removeDonor_handler = CommandHandler("removeDonor", removeDonor)
    dispatcher.add_handler(removeDonor_handler)

    leaderboard_handler = CommandHandler("leaderboard", leaderboard)
    dispatcher.add_handler(leaderboard_handler)

    trustwallet_handler = CommandHandler("tw", tw)
    dispatcher.add_handler(trustwallet_handler)

    exchanges_handler = CommandHandler("exchanges", exchanges)
    dispatcher.add_handler(exchanges_handler)

    gamble_handler = CommandHandler("gamble", gamble)
    dispatcher.add_handler(gamble_handler)

    rain_handler = CommandHandler("rain", rain)
    dispatcher.add_handler(rain_handler)

    check_deposit_transactions_handler = CommandHandler("check_deposit_transactions", check_deposit_transactions)
    dispatcher.add_handler(check_deposit_transactions_handler)

    check_stake_transactions_handler = CommandHandler("check_stake_transactions", check_stake_transactions)
    dispatcher.add_handler(check_stake_transactions_handler)

    # Add error handler.
    dispatcher.add_error_handler(error)

    # Start the Bot.
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

    # This is here to simulate application activity (which keeps the main thread alive).
    try:
        while scheduler_active:
            time.sleep(1)
    except (KeyboardInterrupt, SystemExit):
        # Not strictly necessary if daemonic mode is enabled but should be done if possible
        scheduler.shutdown()

if __name__ == '__main__':
    main()
